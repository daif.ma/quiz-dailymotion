<?php

namespace Core\Model;


use Core\Tools\StringTools;

class BaseRepository
{
    private $db;
    private $classRepo;
    protected $className;
    protected $tableName;

    public function __construct()
    {
        $this->db = Database::getInstance();
        $explode = explode( '\\', get_class($this));
        $this->classRepo = end($explode);
        $this->className = str_replace("Repository", "", $this->classRepo);
        $this->tableName = StringTools::decamelize($this->className);
    }

    /**
     * @return array
     */
    public function findAll()
    {
        $res = $this->query("SELECT * FROM $this->tableName");
        $entities = [];
        foreach ($res as $stdClass) {
            $entities[] = $this->mapToEntity($stdClass);
        }
        return $entities;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        $sql = "SELECT * FROM $this->tableName WHERE id = :id";

        $sth = $this->getPDO()->prepare($sql);
        $params[':id'] = $id;
        $sth->execute($params);
        $res = $sth->fetchAll(\PDO::FETCH_OBJ);
         return $this->mapToEntity(reset($res));
    }

    public function query($statement)
    {
        return $this->getPDO()->query($statement)->fetchAll(\PDO::FETCH_OBJ);
    }

    /**
     * Return instance of PDO
     * @return \PDO
     */
    public function getPDO()
    {
        return $this->db->getPDO();
    }

    /**
     * Map Entity to class
     * @param $stdClass
     * @return mixed
     */
    public function mapToEntity($stdClass)
    {
        $sEntity = "App\\Entity\\$this->className";
        $entity = new $sEntity();
        foreach (get_object_vars($stdClass) as $attr => $val) {
            $method = 'set'.ucfirst(StringTools::camelize($attr));
            $entity->$method($val);
        }
        return $entity;
    }
}