<?php

namespace Core;


use Core\Exception\ContainerException;
use Core\Tools\StringTools;
use Symfony\Component\Yaml\Yaml;

class Container
{
    public static $services = [];
    public static $confServices;
    public static $repositories = [];
    public static $_instance;

    /**
     * @param $name
     * @return mixed
     * @throws ContainerException
     */
    public static function get($name)
    {
        if(array_key_exists($name, self::$services)) {
            return self::$services[$name];
        }

        if(!array_key_exists($name, self::$confServices)) {
            throw new ContainerException("No definition for service ($name)");
        }
        $serviceClass = self::$confServices[$name]['class'];
        self::$services[$name] = new $serviceClass();

        return self::get($name);
    }

    /**
     * @param $className
     * @return mixed
     * @throws ContainerException
     */
    public static function getRepository($className)
    {
        if(array_key_exists($className, self::$repositories)) {
            return self::$repositories[$className];
        }
        $repoName = ucfirst(StringTools::camelize($className)).'Repository';

        if(!file_exists(APP_FOLDER.'/src/Repository/'.$repoName.'.php')) {
            throw new ContainerException('File not found '.APP_FOLDER.'/src/Repository/'.$repoName.'.php');
        }
        $repositoryClass = "App\\Repository\\".$repoName;
        self::$repositories[$className] = new $repositoryClass();
        return self::getRepository($className);
    }

    public static function register()
    {
        if(!self::$_instance) {
            // parse config services file
            $serviceConfig = Yaml::parseFile(APP_FOLDER.'/config/services.yml');
            // create container instance
            self::$_instance = new Container();
            self::$confServices = $serviceConfig;
        }
    }


}