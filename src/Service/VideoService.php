<?php

namespace App\Service;


use App\Repository\VideoRepository;
use Core\Container;

class VideoService
{
    /**
     * Get list of videos
     * @return array
     */
    public function getList()
    {
        /** @var VideoRepository $repoVideo */
        $repoVideo = Container::getRepository('video');
        return $repoVideo->findAll();
    }

    /**
     * @param $id integer
     * @return array
     */
    public function get($id)
    {
        /** @var VideoRepository $repoVideo */
        $repoVideo = Container::getRepository('video');
        return $repoVideo->find($id);
    }
}