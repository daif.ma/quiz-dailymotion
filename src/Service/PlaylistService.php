<?php

namespace App\Service;


use App\Entity\Playlist;
use App\Entity\PlaylistVideo;
use App\Repository\PlaylistRepository;
use App\Repository\PlaylistVideoRepository;
use Core\Container;

class PlaylistService
{

    /**
     * Get list of playlist
     * @return array
     */
    public function getList()
    {
        /** @var PlaylistRepository $repoPlaylist */
        $repoPlaylist = Container::getRepository('playlist');
        return $repoPlaylist->findAll();
    }

    /**
     * @param $id
     * @return Playlist
     */
    public function get($id)
    {
        /** @var PlaylistRepository $repoPlaylist */
        $repoPlaylist = Container::getRepository('playlist');
        return $repoPlaylist->find($id);
    }

    /**
     * get Videos of playlist
     * @param $playlistId
     * @return array
     */
    public function getVideos($playlistId)
    {
        /** @var PlaylistVideoRepository $repoPlaylist */
        $repoPlaylist = Container::getRepository('playlist_video');
        /** @var PlaylistVideo[] $entities */
        $entities = $repoPlaylist->findByPlaylistId($playlistId);

        // Format data
        $formattedData = [];
        /** @var PlaylistVideo $entity */
        foreach ($entities as $entity) {
            $formattedData[] = $entity->formatData();
        }
        return $formattedData;
    }

    /**
     * Create playlist
     * @param $name
     * @return bool
     */
    public function create($name)
    {
        /** @var PlaylistRepository $repoPlaylist */
        $repoPlaylist = Container::getRepository('playlist');
        $playlist = new Playlist();
        $playlist->setName($name);
        return $repoPlaylist->add($playlist);
    }

    /**
     * update playlist
     * @param $id
     * @param $name
     * @return bool
     */
    public function update($id, $name)
    {
        /** @var PlaylistRepository $repoPlaylist */
        $repoPlaylist = Container::getRepository('playlist');

        $playlist = new Playlist();
        $playlist->setId($id);
        $playlist->setName($name);

        return $repoPlaylist->update($playlist);
    }

    /**
     * Add video to playlist
     * @param $idPlaylist
     * @param $idVideo
     * @return bool
     */
    public function addVideoToPlaylist($idPlaylist, $idVideo)
    {
        /** @var PlaylistVideoRepository $repoPlaylistV */
        $repoPlaylistV = Container::getRepository('playlist_video');
        if(!$repoPlaylistV->videoExistInPlaylist($idPlaylist, $idVideo))
            return $repoPlaylistV->addVideoToPlaylist($idPlaylist, $idVideo);
        return false;
    }
}