<?php

namespace App\Repository;


use App\Entity\Playlist;
use Core\Model\BaseRepository;

class PlaylistRepository extends BaseRepository
{
    /**
     * create playlist
     * @param $playlist
     * @return bool
     */
    public function add($playlist)
    {
        $sql = 'INSERT INTO playlist (name) VALUES (:name)';
        $sth = $this->getPDO()->prepare($sql);
        $code = $sth->execute([':name' =>  $playlist->getName()]);
        return $code;
    }

    /**
     * update playlist
     * @param $playlist Playlist
     * @return bool
     */
    public function update($playlist)
    {
        $sql = 'UPDATE playlist SET name=:name WHERE id = :id';
        $sth = $this->getPDO()->prepare($sql);
        $code = $sth->execute([':id' =>  $playlist->getId(), ':name' => $playlist->getName()]);
        return $code;
    }

    /**
     * Delete playlist
     * @param $id
     * @return bool
     */
    public function delete($id)
    {
        $sql = 'DELETE FROM playlist_video WHERE playlist_id = :playlist_id';
        $sth = $this->getPDO()->prepare($sql);
        $params[':playlist_id'] = $id;
        $code = $sth->execute($params);
        if(!$code) return false;
        $sql = 'DELETE FROM playlist WHERE id = :playlist_id';
        $sth = $this->getPDO()->prepare($sql);
        $params[':playlist_id'] = $id;
        $code = $sth->execute($params);
        if(!$code) return false;
        return true;
    }
}